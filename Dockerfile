FROM 944904453359.dkr.ecr.us-east-2.amazonaws.com/testrepo:latest
EXPOSE 5762
COPY . /usr/local/src/myscripts
WORKDIR /usr/local/src/myscripts
CMD [ "bash" ]